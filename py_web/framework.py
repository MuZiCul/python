# -*- coding: utf-8 -*-
"""处理动态资源请求"""
import pymysql
import json
import logging


# 路由列表
route_list = [
    # ("/index.html", index),
    # ("/center.html", center),
]


# 定义带有参数的装饰器来做路由
def route(path):
    # 装饰器
    def decorator(func):
        # 当装饰器执行的时候，就需要把路由添加到路由列表里面，当装饰的时候只添加一次路由即可
        # 在服务启动的时候装饰器自动将添加路由到路由列表
        route_list.append((path, func))

        def inner():
            result = func()
            return result
        return inner
    # 返回装饰器
    return decorator


@route("/index.html")
def index():
    # 状态信息
    status = "200 OK"
    # 响应头
    response_header = [("Server", "PWS/1.0")]
    # web框架处理后的数据
    with open("template/index.html", "r", encoding="utf-8") as file:
        file_data = file.read()
    # 创建数据库对象
    conn = pymysql.connect(host="localhost",
                           port=3306,
                           user="root",
                           password='',
                           database="web",
                           charset="utf8",)
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select * from info;"
    # 执行sql
    cursor.execute(sql)
    # 获取查询结果
    result = cursor.fetchall()
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()
    # 遍历每一条数据，完成数据的tr标签的封装
    data = ""
    for row in result:
        data += """<tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td><input type="button" value="添加" id="toAdd" name="toAdd" systemidvaule="000007"></td>
                   </tr>""" % row
    response_body = file_data.replace("{%content%}", data)
    # 以元组形式返回
    return status, response_header, response_body


# 个人中心数据接口
@route("/center_data.html")
def center_data():
    # 创建数据库对象
    conn = pymysql.connect(host="localhost",
                           port=3306,
                           user="root",
                           password='',
                           database="web",
                           charset="utf8",
                           )
    # 获取游标
    cursor = conn.cursor()
    # 准备sql
    sql = "select i.code, i.short, i.chg, i.turnover, i.price, i.highs, s.note_info from info i inner join focus s on i.id = s.info_id;"
    # 执行sql
    cursor.execute(sql)
    # 获取查询结果，元组类型
    result = cursor.fetchall()
    # 将元组转换成字典
    center_data_list = [{"code": row[0],
                        "short": row[1],
                         "chg": row[2],
                         "turnover": row[3],
                         "price": str(row[4]),
                         "highs": str(row[5]),
                         "note_info": row[6],
                         } for row in result
                        ]
    # 把列表转换为json字符串
    json_str = json.dumps(center_data_list, ensure_ascii=False)
    # 关闭游标
    cursor.close()
    # 关闭连接
    conn.close()
    # 状态信息
    status = "200 OK"
    # 响应头
    response_header = [
        ("Server", "PWS/1.0"),
        ("Content-Type", "text/html; charset=utf-8")
    ]
    return status, response_header, json_str


@route("/center.html")
def center():
    # 状态信息
    status = "200 OK"
    # 响应头
    response_header = [("Server", "PWS/1.0")]
    # web框架处理后的数据
    with open("template/center.html", "r", encoding="utf-8") as file:
        file_data = file.read()

    response_body = file_data.replace("{%content%}", "")
    # 以元组形式返回
    return status, response_header, response_body


@route("/error.html")
def error():
    # 状态信息
    status = "404 Not Found"
    # 响应头
    response_header = [("Server", "PWS/1.0")]
    # web框架处理后的数据
    with open("static/error.html", "r", encoding="utf-8") as file:
        response_body = file.read()
    # 以元组形式返回
    return status, response_header, response_body


# 动态处理资源请求
def handle_request(env):
    # 获取资源路径
    request_path = env["request_path"]
    # 便利路由列表，匹配请求的url
    for path, func in route_list:
        if request_path == path:
            result = func()
            return result
    else:
        logging.error(f"不存在资源：{request_path}")
        result = error()
        return result


if __name__ == '__main__':
    center_data()
