import socket
import sys
import threading
import logging


# 设置logging日志的配置信息
# level 表示设置级别
# %(asctime)s 表示当前时间
# %(filename)s 表示程序文件名
# %(lineno)d 表示行号
# %(levelname)s 表示日志级别
# %(message)s 表示日志信息
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s-%(filename)s[lineno:%(lineno)d]-%(levelname)s-%(message)s",
                    filename="log.txt",
                    filemode="a")


class HttpWebServer(object):
    def __init__(self, port):
        # 创建tcp服务端套接字
        tcp_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # 设置端口号复用，程序退出端口号立即释放
        tcp_server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        # 绑定端口号
        tcp_server_socket.bind(("", port))
        # 设置监听
        tcp_server_socket.listen(128)
        print("服务器已启动等待接收客户端信息！")
        # 把tcp服务器的套接字作为web服务器对象的属性
        self.tcp_server_socket = tcp_server_socket

    def start(self):
        # 循环等待接收客户端的连接请求
        while True:
            # 等待接收客户端的连接请求
            new_socket, ip_port = self.tcp_server_socket.accept()
            # 连接建立成功，多线程处理客户端请求
            sub_thread = threading.Thread(target=self.handle_client_request, args=(new_socket, ip_port))
            # 守护线程
            sub_thread.setDaemon(True)
            # 启动线程
            sub_thread.start()

    @staticmethod
    def handle_client_request(new_socket, ip_port):
        # 接收客户端的请求信息,4096表示接受的数据包大小，浏览器GET请求一般上限为4KB
        recv_data = new_socket.recv(4096)
        # print(f"请求信息：{recv_data}")
        # 解决请求为空的情况
        if recv_data is None:
            new_socket.close()
            return
        # 对接受的请求信息进行格式化
        recv_connect = recv_data.decode("utf-8")
        # print(f"格式化后请求信息：{recv_connect}")
        # 对格式化数据按照空格进行分割
        request_list = recv_connect.split(" ", 2)
        # print(f"分割后数据：{request_list}")
        # 获取请求类型
        request_type = request_list[0]
        # 获取请求资源路径
        request_path = request_list[1]
        # 设置默认主页面
        if request_path == "/":
            request_path = "/index.html"
        # 判断文件是否存在
        # os.path.exists("static" + request_path)
        try:
            # 打开文件，读取文件
            with open("static" + request_path, "rb") as file:
                file_data = file.read()
        except Exception as e:
            print(f"{ip_port}正在请求不存在资源路径：{request_path}，返回资源error.html")
            with open("static/error.html", "rb") as file:
                file_data = file.read()
            # 封装http响应报文数据
            # 响应行
            response_line = "HTTP/1.1 404 Not Found\r\n"
            # 响应头
            response_header = "Server: PWS/1.0\r\n"
            # 空行
            # 响应体
            response_body = file_data
            response_data = (response_line + response_header + "\r\n").encode("utf-8") + response_body
            # 发送给浏览器的响应报文
            new_socket.send(response_data)
        else:
            print(f"{ip_port}正在请求存在资源路径：{request_path}")
            # 封装http响应报文数据
            # 响应行
            response_line = "HTTP/1.1 200 OK\r\n"
            # 响应头
            response_header = "Server: PWS/1.0\r\n"
            # 空行
            # 响应体
            response_body = file_data
            response_data = (response_line + response_header + "\r\n").encode("utf-8") + response_body
            # 发送给浏览器的响应报文
            new_socket.send(response_data)
        finally:
            # 关闭服务于客户端的套接字
            new_socket.close()


def web_server():
    # 获取终端命令参数
    params = sys.argv
    # 判断命令格式
    if len(params) != 2:
        print("命令格式有误，例如：python xxx.py 8000")
        logging.warning("命令格式有误，长度不等于2")
        return
    # 判断第二个参数[端口号]是否为数字
    if not params[1].isdigit():
        print("命令格式有误，例如：python xxx.py 8000")
        logging.warning("命令格式有误，第二个参数非数字")
        return
    port = int(params[1])
    # 创建web服务器
    web_Server = HttpWebServer(port)
    # 启动服务器
    web_Server.start()


if __name__ == '__main__':
    web_server()
