import copy
import random
import re
import os
import sys
import time
import concurrent.futures
import shutil
import DB
import utils
import config

Fore = config.Fore
localePath = ''

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'accept-language': 'zh-CN,zh;q=0.9',
    "host": "t66y.com",
    'Connection': 'keep-alive',
    "Content-Type": "application/json;charset=utf-8",
    'Accept-Encoding': 'gzip, deflate',
    'Upgrade-Insecure-Requests': '1'
}

headers_User_Agent = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
}

headers1 = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'If-None-Match': "62519b25-7c336b"
}

headers2 = {
    'Host': 'louimg.com',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1'
}

# 失败列表
FailUrlList = []
# 根目录
timeout = 15
proxies = config.proxies


def DownLoadImg(url, path, SN, No):
    # 修改请求头，防止被服务器屏蔽
    global headers1
    global FailUrlList
    # 获取图片后缀
    others = url.rsplit('.', 1)
    img_suffix = others[1]
    if 'svg' in img_suffix:
        return
    # 拼接图片真实存放地址
    if No == 1:
        rulePath = path + '\\' + str(others[0].rsplit('/', 1)[1]) + '.' + img_suffix
    else:
        rulePath = path + '\\' + SN + '.' + img_suffix

    response = utils.getResponse(url, headers1, 1)
    if response:
        with open(rulePath, 'wb') as img_file:
            img_file.write(response[2])  # 写入二进制内容
            print(Fore.GREEN + f"No.{str(SN).zfill(3)}:{url} ,Download Successful,Status Code:{response[0]}")
    else:
        print(Fore.RED + f'\n下载第{SN}个：{url}时出错。\n')
        FailUrlList.append(url)


def createFolder(tittle):
    sets = ['/', '\\', ':', '*', '?', '"', '<', '>', '|', '\t']
    for char in tittle:
        if char in sets:
            tittle = tittle.replace(char, '_')
    paths = localePath + '\\' + tittle
    # 获取目录下文件夹
    dir_list = []
    for root, dirs, files in os.walk(localePath):
        dir_list = dirs
        break
    for dir_one in dir_list:
        if tittle in dir_one:
            print(Fore.YELLOW + f'相似文件夹{dir_one}已存在，图片将存到本目录下！')
            paths = localePath + '\\' + dir_one
            break
    else:
        os.mkdir(paths)
        print(Fore.GREEN + f'创建文件夹-->{paths}<--成功！')
    return paths


def analWebData(html):
    # 正则匹配图片链接和主题名
    urlLists = utils.getUrlList('ess-data=\'(.*?)\'', html)
    tittle = utils.getUrlList('class="f16">(.*?)</h4>', html)
    if len(urlLists) < 1:
        print(Fore.RED + '图片列表获取失败，请检查Url！')
        return False, False
    else:
        global headers1
        if 's3.xoimg.com' in urlLists[0]:
            headers1 = headers1
        elif 'louimg.com' in urlLists[0]:
            headers1 = headers2
        else:
            headers1 = headers_User_Agent
        if utils.checkNet(urlLists[0], len(urlLists), headers1):
            # 创建目录文件夹
            realPath = createFolder(tittle[0])
            return urlLists, realPath
        else:
            return False, False


def DownLoadImgList(urlList, path, max_workers):
    print(Fore.PURPLE + f'开始下载{len(urlList)}个文件：')
    No = len(urlList)
    exe = concurrent.futures.ThreadPoolExecutor(max_workers=max_workers)
    for url in urlList:
        exe.submit(DownLoadImg, url, path, str(len(urlList) - No + 1), 0)
        No -= 1
    exe.shutdown()
    print(Fore.PURPLE + f'\n下载完成！'
          f'\n下载{len(urlList)}个图片，'
          f'\n下载成功：{len(urlList) - len(FailUrlList)}个，'
          f'\n下载失败：{len(FailUrlList)}个，'
          f'\n存放位置：{path}')


def DownLoadStart(URL, downloadPath):
    if downloadPath != '':
        global localePath
        localePath = downloadPath
    global FailUrlList
    # 修改请求头，防止被服务器屏蔽
    global headers
    response = utils.getResponse(URL, headers, 0)
    if not response:
        return False
    urlList, path = analWebData(response[1])
    if urlList:
        DownLoadImgList(urlList, path, 10)
        DownloadFailList(path)
        if not Judge_picture_quality(path):
            return False
        return True
    else:
        return False


def DownloadFailList(path):
    while len(FailUrlList) > 0:
        print(Fore.BLUE + f'\n下载失败列表：')
        for failUrl in FailUrlList:
            print(Fore.RED + failUrl)
        failCount = 0
        while len(FailUrlList) > 0:
            if failCount > 4:
                if FailUrlList:
                    for url in FailUrlList:
                        dataBase.insertFailImgUrl(url)
                FailUrlList.clear()
                return
            FailList = copy.deepcopy(FailUrlList)
            FailUrlList.clear()
            print(Fore.BLUE + f'\n开始重新下载失败列表{len(FailList)}个文件：')
            DownLoadImgList(FailList, path, len(FailList))
            failCount += 1


def Start():
    while True:
        URL = input(Fore.GREEN + '请输入贴吧链接（输入”0“停止运行）：')
        if URL == '0':
            print(Fore.RED + '\n程序结束运行！')
            sys.exit()
        tic = time.time()
        regex = re.compile(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if re.match(regex, URL) is not None:
            if 't66y' in URL:
                if 'localhost' in URL or 't66y' not in URL:
                    print(Fore.RED + "仅支持草榴社区，新时代的我们中帖子Url，请检查Url后重新输入。\n")
                    continue
                if not 199 < utils.getResponse(tage, headers, 0)[0] < 300:
                    print(Fore.RED + "请求获取失败，请检查Url后重新输入。\n")
                    continue
                if utils.judgeUrlDownloadSuccess(dataBase, URL):
                    continue
                else:
                    if DownLoadStart(URL, ''):
                        dataBase.insertSuccessPageUrl(URL)
            elif 'yalayi' in URL:
                pass
            else:
                print(Fore.RED + "Url无法识别，请重新输入。\n")
                continue
            toc = time.time()
            print(Fore.PURPLE + f'\n本次用时：{round(toc - tic, 2)}秒\n')
        else:
            print(Fore.RED + "Url有误，请检查Url后重新输入。\n")
            continue


def Judge_picture_quality(start_path):
    total_size = 0
    total = 0
    quality = ''
    gif = 0
    DocumentQuality = config.DQ
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            if '.gif' in f:
                gif = 1
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total += 1
                total_size += os.path.getsize(fp)
    if total == 0:
        os.remove(start_path)
        return False
    totalMb = round(total_size / 1048576, 2)
    filesize = round(totalMb / total, 2)

    if filesize < 0.11:
        quality = DocumentQuality[0]
    elif 0.1 < filesize < 0.6:
        quality = DocumentQuality[1]
    elif 0.5 < filesize < 1.6:
        quality = DocumentQuality[2]
    elif 1.5 < filesize < 2.1:
        quality = DocumentQuality[3]
    elif 2 < filesize < 5.1:
        quality = DocumentQuality[4]
    elif 5 < filesize < 10.1:
        quality = DocumentQuality[5]
    elif 10 < filesize < 15.1:
        quality = DocumentQuality[6]
    elif 15 < filesize < 20.1:
        quality = DocumentQuality[7]
    elif 20 < filesize:
        quality = DocumentQuality[8]
    if gif == 1:
        quality = DocumentQuality[9]
    start_path_rsplit = start_path.rsplit('\\', 1)
    dir_path = start_path_rsplit[1]
    new_path = start_path_rsplit[0] + f'\\[{quality}][AVG：{filesize}MB][' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + ']'

    if '质量' in start_path and 'AVG' in start_path:
        file_name = ''
        for dir_ in range(30, len(dir_path)):
            file_name += dir_path[dir_]
        new_path += file_name
    else:
        new_path += dir_path

    os.rename(start_path, new_path)
    if not Inductive(new_path, filesize, gif):
        return False
    print(Fore.PURPLE + f'\n图片综合质量评价：\n 图片总数：{total}个\n 整体质量：{totalMb}MB\n 平均质量：{filesize}MB\n 综合评价：{quality}')
    return True


# 按图片质量归类
def Inductive(PrFilePath, filesize, gif):
    fileList = []
    oldPathRsplit = PrFilePath.rsplit('\\', 2)
    Inductive_Catalog = utils.inductive(filesize, oldPathRsplit, gif)
    targetPath = oldPathRsplit[0] + '\\' + Inductive_Catalog
    targetDir = targetPath + '\\' + PrFilePath.rsplit('\\', 1)[1]
    if os.path.exists(targetDir):
        print(Fore.RED + f'归档文件已存在，已停止归档')
        return False
    if os.path.exists(targetPath):
        shutil.move(PrFilePath, targetPath)
    else:
        os.mkdir(targetPath)
        shutil.move(PrFilePath, targetPath)

    if not os.path.exists(targetDir + '\\手机壁纸'):
        os.mkdir(targetDir + '\\手机壁纸')
    if not os.path.exists(targetDir + '\\电脑壁纸'):
        os.mkdir(targetDir + '\\电脑壁纸')
    for root, dirs, files in os.walk(targetDir):
        fileList = files
        break
    for file in fileList:
        if utils.getImgResolution(targetDir + '\\' + file):
            shutil.move(targetDir + '\\' + file, targetDir + '\\电脑壁纸')
        else:
            shutil.move(targetDir + '\\' + file, targetDir + '\\手机壁纸')
    print(Fore.PURPLE + f'已归入文件夹：{Inductive_Catalog}')
    return True


def Crawlall(SN, UrlType):
    global FailUrlList
    # 修改请求头，防止被服务器屏蔽
    global headers
    # 爬取网页内容
    URL = f'http://t66y.com/thread0806.php?fid=16&search=&page={SN}'
    response = utils.getResponse(URL, headers, 0)
    if not response:
        return False
    # 正则匹配图片链接和主题名
    result = utils.getUrlList('<td class="tal" id=""> \r\n\r\n\t\r\n\r\n\t(.*?)\r\n\r\n\t<h3><a href="(.*?)" target="_blank" id="">(.*?)</a></h3>', str(response[1]))
    count = 1
    for res in result:
        print(f'下载第{SN}页，第{count}条')
        count += 1
        if UrlType in res[0]:
            if not dataBase.selectSuccessPageUrl(res[1]):
                if not DownLoadStart(f'http://t66y.com/{res[1]}', ''):
                    dataBase.insertFailPageUrl(res[1])
                else:
                    dataBase.insertSuccessPageUrl(res[1])


if __name__ == '__main__':
    while True:
        tage = input(Fore.GREEN + '是否进行批量下载（0/1）：')
        dataBase = DB.Operation_mysql(True)
        if tage == '1':
            localePath = 'E:\\自拍\\未分类或已损坏'
            startPage = input(Fore.YELLOW + '开始页码：')
            endPage = input(Fore.YELLOW + '结束页码：')
            for i in range(int(startPage), int(endPage)):
                print(Fore.BLUE + f'开始下载第{i}页：')
                Crawlall(i, '')
                print(Fore.BLUE + f'第{i}页下载完成')
        else:
            localePath = 'E:\\草料\\临时\\未分类或已损坏'
            Start()
        dataBase.close_conn()

