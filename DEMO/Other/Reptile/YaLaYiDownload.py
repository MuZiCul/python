import copy
import random
import requests
import re
import os
import sys
import time
import concurrent.futures
from datetime import datetime
import DownLoadImg

headers = {
    'Host': 'www.yalayi.com',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'If-None-Match': 'W/"6268ea8c-3d7a"'
}

headerImg = {
    'Host': 'img.yalayi.net',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'If-None-Match': "17E13EB3DDC6379A31876F8C58867171"
}

User_Agents = [
    "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14",
    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)",
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
    'Opera/9.25 (Windows NT 5.1; U; en)',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
    'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
    'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
    'Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/1.2.9',
    "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Ubuntu/11.04 Chromium/16.0.912.77 Chrome/16.0.912.77 Safari/535.7",
    "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:10.0) Gecko/20100101 Firefox/10.0 "
]

# 失败列表
FailUrlList = []
# 根目录
localePath = 'D:\\草料\\YaLaYi'


def DownLoadImg(url, path, SN, No):
    # 修改请求头，防止被服务器屏蔽
    ChangeHeader(headers)
    global FailUrlList
    try:
        # 获取图片后缀
        others = url.rsplit('.', 1)
        img_suffix = others[1]
        # 拼接图片真实存放地址
        if No == 1:
            rulePath = path + '\\' + str(others[0].rsplit('/', 1)[1]) + '.' + img_suffix
        else:
            rulePath = path + '\\' + SN + '.' + img_suffix
        # 发送get请求图片url,
        # 第一个参数为图片链接，
        # 第二个为请求头，
        # 第三个为代理服务器（因为国内无法访问草榴社区，本人本地用clash.net来代理，代理端口为11223）
        headers["Host"] = 'img.yalayi.net'
        r = requests.get(url, headers=headers, timeout=15)
        if r.status_code == 200:
            with open(rulePath, 'wb') as img_file:
                img_file.write(r.content)  # 写入二进制内容
                print(f"No.{str(SN).zfill(3)}:{url} ,Download Successful,Time Cost:{round(r.elapsed.total_seconds(), 2)}s,Status Code:{r.status_code}")
        else:
            print(f'\n下载第{SN}个：{url}时出错。\n故障代码：{r.status_code}\n')
            FailUrlList.append(url)
    except Exception as e:
        print(f'\n下载第{SN}个：{url}时出错。\n故障详情：{e}\n')
        FailUrlList.append(url)


def createFolder(tittle):
    sets = ['/', '\\', ':', '*', '?', '"', '<', '>', '|', '\t']
    for char in tittle:
        if char in sets:
            tittle = tittle.replace(char, '_')
    paths = localePath + '\\[' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + ']' + tittle
    # 获取目录下文件夹
    dir_list = []
    for root, dirs, files in os.walk(localePath):
        dir_list = dirs
        break
    dir_str = ''
    for i in dir_list:
        dir_str += i
    if os.path.exists(paths):
        now = datetime.now()
        suffix = f'_{now.year:04d}{now.month:02d}{now.day:02d}{now.hour:02d}{now.minute:02d}{now.second:02d}'
        paths = paths + suffix
        print(f'原文件夹已存在，重新生成为{paths}，图片将存到本目录下！')
    elif tittle in dir_str:
        nama_index = dir_str.index(tittle)
        name_len = len(tittle)
        file_name = ''
        for i in range(nama_index - 30, nama_index + name_len):
            file_name += dir_str[i]
        paths = localePath + '\\' + file_name
        if os.path.exists(paths):
            now = datetime.now()
            suffix = f'_{now.year:04d}{now.month:02d}{now.day:02d}{now.hour:02d}{now.minute:02d}{now.second:02d}'
            paths = paths + suffix
            print(f'原文件夹已存在，重新生成为{paths}，图片将存到本目录下！')
    os.mkdir(paths)
    print(f'创建文件夹-->{paths}<--成功！')
    return paths


def analWebData(html):
    pcImg = '!pcimg'
    real_urlLists = []
    urlLists = re.findall('<img class=\'lazy\' src=\'(.*?)\' data-original=\'(.*?)\'', html, re.S)
    for url in urlLists:
        if pcImg in url[0]:
            if 'end' in url[0]:
                continue
            real_urlLists.append(url[0].replace(pcImg, ''))
        else:
            if 'end' in url[1]:
                continue
            real_urlLists.append(url[1].replace(pcImg, ''))
    tittle = re.findall('alt="《(.*?)》作品封面图" >', html, re.S)
    tittle_1 = re.findall('pcimg\' alt="(.*?)作品封面图" >', html, re.S)
    if len(tittle) < 1:
        tittle = copy.deepcopy(tittle_1)
    if len(real_urlLists) < 1:
        print('图片链接获取失败，请检查Url！')
        return real_urlLists, 0
    elif len(tittle) < 1:
        now = datetime.now()
        suffix = f'_{now.year:04d}{now.month:02d}{now.day:02d}{now.hour:02d}{now.minute:02d}{now.second:02d}'
        tittles = 'yalayi' + suffix
        print(f'图片链接获取成功，共获取到{len(real_urlLists)}个图片链接，但未获取到图片标题，将使用当前时间生成一个标题')
        # 创建目录文件夹
        realPath = createFolder(tittles)
        return real_urlLists, realPath
    else:
        print(f'图片链接获取成功，共获取到{len(real_urlLists)}个图片链接！')
        # 创建目录文件夹
        realPath = createFolder(tittle[0])
        return real_urlLists, realPath


def checkNet(urls, url_list_len):
    print('开始检查网络连通性：')
    start = 0
    end = 0
    rs = ''
    try:
        start = time.time()
        headers["Host"] = 'img.yalayi.net'
        rs = requests.get(urls, headers=headers, timeout=15)
        end = time.time()
    except Exception as es:
        if 'proxy' in str(es) or 'Proxy' in str(es):
            print(f'网络异常，请检查代理！\n检查用时{end-start}s\n故障详情：{es}')
            sys.exit()
    if rs.reason == 'OK':
        print(f'网络正常，请求正常，可以进行正常下载！\n状态码：{rs.status_code}\n预计下载时间：{round(url_list_len*(end-start)/2, 2)}s')
        if round(url_list_len*(end-start)/2, 2) > 60:
            if input('预估下载时间超过1分钟，可能下载失败，是否继续(0/1)？') == '0':
                Start()
        return 1
    else:
        print(f'网络异常，请检查！\n故障详情{rs}')
        sys.exit()


def DownLoadImgList(urlList, path):
    print(f'开始下载{len(urlList)}个文件：')
    No = len(urlList)
    exe = concurrent.futures.ThreadPoolExecutor(max_workers=4)
    for i in urlList:
        exe.submit(DownLoadImg, i, path, str(len(urlList) - No + 1), 0)
        No -= 1
    exe.shutdown()
    print(f'\n下载完成！'
          f'\n下载{len(urlList)}个图片，'
          f'\n下载成功：{len(urlList) - len(FailUrlList)}个，'
          f'\n下载失败：{len(FailUrlList)}个，'
          f'\n存放位置：{path}')


def DownLoadStart(URL):
    global FailUrlList
    # 修改请求头，防止被服务器屏蔽
    global headers
    ChangeHeader(headers)
    # 爬取网页内容
    headers["Host"] = 'www.yalayi.com'
    res = requests.get(URL, headers=headers, timeout=15)
    res.encoding = 'utf-8'
    # 解析网页内容
    urlList, path = analWebData(str(res.text))
    if path == '0' or path == 0:
        print('\n获取链接异常，请检查！')
        return '0'
    # 检查网络连通性
    # if checkNet(urlList[0], len(urlList)):
    print(f'网络连通性检查完成！\n文件列表获取完成，共{len(urlList)}个文件！\n')
    DownLoadImgList(urlList, path)
    errorCount = 0
    if len(FailUrlList) > 0:
        print(f'\n下载失败列表：')
        for i in FailUrlList:
            print(i)
        while len(FailUrlList) > 0:
            if errorCount == 5:
                FailUrlList.clear()
                break
            FailList = copy.deepcopy(FailUrlList)
            FailUrlList.clear()
            print(f'\n开始重新下载失败列表{len(FailList)}个文件：')
            No = len(FailList)
            for i in FailList:
                DownLoadImg(i, path, str(len(FailList) - No + 1), 1)
                No -= 1
                errorCount += 1
    return path
    # else:
    #     print('\n网络连通性异常，请检查！')
    #     return '0'


def Start():
    # 已下载到961，下次从961开始
    for page in range(0, 999):
        # tage = input('请输入贴吧链接（输入”0“停止运行）：')
        tage = f'https://www.yalayi.com/gallery/{page}.html'
        if tage == '0':
            print('\n程序结束运行！')
            sys.exit()
        tic = time.time()
        regex = re.compile(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if re.match(regex, tage) is not None:
            if 'yalayi' in tage:
                if 'localhost' in tage:
                    print("Url无法识别，请重新输入。\n")
                    continue
                headers["Host"] = 'www.yalayi.com'
                if requests.get(tage, headers=headers, timeout=15).status_code != 200:
                    print("请求获取失败，请检查Url后重新输入。\n")
                    continue
                path = DownLoadStart(tage)
                if path != '0':
                    DownLoadImg.Judge_picture_quality(path)
            else:
                print("Url无法识别，请重新输入。\n")
                continue
            toc = time.time()
            print(f'\n本次用时：{round(toc - tic, 2)}秒\n')
        else:
            print("Url有误，请检查Url后重新输入。\n")
            continue


def ChangeHeader(header):
    header["User-Agent"] = random.choice(User_Agents)


if __name__ == '__main__':
    tic = time.time()
    Start()
    toc = time.time()
    print(f'\n全部下载，总用时：{round(toc - tic, 2)}秒\n')
