import os
import shutil
import time


def remove(PrFilePath, targetPath):
    if os.path.exists(targetPath):
        shutil.move(PrFilePath, targetPath)
        return 1
    else:
        os.mkdir(targetPath)
        shutil.move(PrFilePath, targetPath)
        return 1


def traFolder(oldRootPath, newRootPath):
    old_dir_list = []
    mid_dir_list = []
    for root, dirs, files in os.walk(oldRootPath):
        old_dir_list = dirs
        break
    for i in range(0, len(old_dir_list)):
        for rootPath, dirsPath, filesPath in os.walk(oldRootPath + '\\' + old_dir_list[i]):
            mid_dir_list = dirsPath
            break
        for old_idr in mid_dir_list:
            PrFilePath = oldRootPath + '\\' + old_dir_list[i] + '\\' + old_idr
            targetPath = newRootPath + '\\' + old_dir_list[i]
            if remove(PrFilePath, targetPath) == 1:
                print(f'{PrFilePath}复制成功！')


if __name__ == '__main__':
    oldFilRootPath = 'D:\\草料\\YaLaYi'
    targetRootPath = 'E:\\草料\\YaLaYi'
    start = time.time()
    traFolder(oldFilRootPath, targetRootPath)
    end = time.time()
    print(f'移动完成，用时：{end - start}')





