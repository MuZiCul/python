import os

import DownLoadImg

'''
检查指定目录下的文件夹内图片，按照定义好的质量标准重命名和归类各文件夹
'''

if __name__ == '__main__':
    # path为要检查的目录
    path = 'E:\\草料\\P'
    # 获取目录下文件夹
    dir_list = []
    for root, dirs, files in os.walk(path):
        dir_list = dirs
        break
    for dir in dir_list:
        print(dir)
        DownLoadImg.Judge_picture_quality(path + '\\' + dir)
