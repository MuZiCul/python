import time

import config
import requests
import re
from PIL import Image
import random

timeout = config.timeout
proxies = config.proxies
Fore = config.Fore
User_Agents = config.User_Agents
imgSuffix = config.imgSuffix
returnType = config.ReturnType


def getResponse(URL, headers, ReturnType):
    timeSleep = 2
    failCount = 0
    if ReturnType == 0:
        timeSleep = 3
    elif ReturnType == 1:
        timeSleep = 1
    while True:
        if failCount > 3:
            print(Fore.RED + f'连接[{URL}]失败，请求次数过三，请求已终止！')
            return False
        try:
            headers["User-Agent"] = random.choice(User_Agents)
            response = requests.get(url=URL, headers=headers, timeout=timeout, proxies=proxies, verify=True)
            # 判断状态码是否是200，自动引发HTTPError
            response.raise_for_status()
            status_code = response.status_code
            text = response.text
            content = response.content
            response.close()
            if 199 < status_code < 300:
                if ReturnType:
                    print(Fore.BLUE + f'连接[{URL}]成功，状态码：' + Fore.YELLOW + f'{status_code}')
                return status_code, text, content
        except requests.exceptions.ConnectTimeout:
            print(f'连接超时，{timeSleep}秒后重新开始！')
            time.sleep(timeSleep)
        except requests.exceptions.ProxyError:
            print(f'代理异常，{timeSleep}秒后重新开始')
            time.sleep(timeSleep)
        except requests.exceptions.ConnectionError:
            print(f'网络异常，{timeSleep}秒后重新开始连接！')
            time.sleep(timeSleep)
        except requests.HTTPError:
            print(f'HTTP错误异常，{timeSleep}秒后重新开始连接！')
            time.sleep(timeSleep)
        except requests.URLRequired:
            print(f'URL缺失异常，{timeSleep}秒后重新开始连接！')
            time.sleep(timeSleep)
        except requests.TooManyRedirects:
            print(f'超过最大重定向次数，产生重定向异常')
        except Exception as e:
            print(Fore.YELLOW + f'连接[{URL}]出现未知异常，错误信息：{e}，{timeSleep}秒后重新开始请求')
            time.sleep(timeSleep)
        finally:
            failCount += 1


def getLocalTime():
    c_time = time.strftime("[%Y-%m-%d %H:%M:%S] ", time.localtime())
    return str(c_time)


def getUrlList(regular, text):
    return re.findall(regular, text, re.S)


def getImgResolution(filename):
    try:
        img = Image.open(filename)
        imgSize = img.size
        img.close()
        if imgSize[0] >= imgSize[1]:
            return True
        else:
            return False
    except Exception as e:
        print(Fore.RED + '识别长宽出错：' + str(e))
        return False


def judgeUrlDownloadSuccess(dataBase, imgUrl):
    if dataBase.selectSuccessPageUrl(imgUrl):
        print(Fore.RED + f"此Url：[{imgUrl}]已下载成功，无需重复下载！\n")
        return True
    else:
        return False


def checkNet(urls, url_list_len, headers1):
    print(Fore.YELLOW + '开始检查网络连通性并获取链接列表：')
    start = 0
    end = 0
    try:
        start = time.time()
        response = getResponse(urls, headers1, 0)
        end = time.time()
        if response:
            print(Fore.GREEN + f'网络连通性检查完成！\n链接列表获取完成，共{url_list_len}个链接！\n')
            print(Fore.GREEN + f'状态码：{response[0]}\n预计下载时间：{round(url_list_len * (end - start) / 2, 2)}s')
            return True
    except Exception as es:
        if 'proxy' in str(es) or 'Proxy' in str(es):
            print(Fore.RED + f'网络异常，请检查代理！\n检查用时{end - start}s\n故障详情：{es}')
            return False
        else:
            print(Fore.RED + f'网络异常！错误信息：{str(es)}')
            return False


def inductive(filesize, oldPathRsplit, gif):
    DocumentQuality = config.DocumentQuality
    DQ = config.DQ
    Inductive_Catalog = ''
    if filesize < 0.11 or DQ[0] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[0]
    elif 0.1 < filesize < 0.6 or DQ[1] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[1]
    elif 0.5 < filesize < 1.6 or DQ[2] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[2]
    elif 1.5 < filesize < 2.1 or DQ[3] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[3]
    elif 2 < filesize < 5.1 or DQ[4] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[4]
    elif 5 < filesize < 10.1 or DQ[5] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[5]
    elif 10 < filesize < 15.1 or DQ[6] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[6]
    elif 15 < filesize < 20.1 or DQ[7] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[7]
    elif 20 < filesize or DQ[8] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[8]
    if gif == 1 or DQ[9] in oldPathRsplit:
        Inductive_Catalog = DocumentQuality[9]
    return Inductive_Catalog
