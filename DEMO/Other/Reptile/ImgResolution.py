import os
import shutil

from PIL import Image

rootDir = ''


def t66yStart(Dir):
    global rootDir
    rootDir = Dir
    getTargetFile()


def wallStart():
    getTargetDirWall()


def getImgResolution(filename):
    try:
        img = Image.open(filename)
        imgSize = img.size
        img.close()
        if imgSize[0] >= imgSize[1]:
            return 1
        else:
            return 0
    except Exception as e:
        print(e)
        return -1


def getTargetFile():
    fileList = []
    targetDirList = getTargetDir()
    for targetDir in targetDirList:
        for root, dirs, files in os.walk(targetDir):
            fileList = files
            break
        if not os.path.exists(targetDir + '\\手机壁纸'):
            os.mkdir(targetDir + '\\手机壁纸')
        if not os.path.exists(targetDir + '\\电脑壁纸'):
            os.mkdir(targetDir + '\\电脑壁纸')
        for file in fileList:
            if getImgResolution(targetDir + '\\' + file) < 0:
                os.remove(targetDir + '\\' + file)
            else:
                if getImgResolution(targetDir + '\\' + file):
                    shutil.move(targetDir + '\\' + file, targetDir + '\\电脑壁纸')
                else:
                    shutil.move(targetDir + '\\' + file, targetDir + '\\手机壁纸')
        print(f'{targetDir}移动完成')

    return fileList


def getTargetDir():
    dir_list = []
    rootList = getRoot()
    resultList = []
    for rootDir in rootList:
        for root, dirs, files in os.walk(rootDir):
            dir_list = dirs
            break
        for i in dir_list:
            resultList.append(rootDir + '\\' + i)
    return resultList


def getTargetDirWall():
    rootList = getRoot()
    resultList = []
    for targetDir in rootList:
        PC_Moble(targetDir)
    return resultList


def getRoot():
    dir_list = []
    resultList = []
    DocumentQuality = ['垃圾质量(AVG：小于0.1M)', '劣质质量(AVG：0.1M-0.6M)', '一般质量(AVG：0.5M-1.6M)',
                       '清晰质量(AVG：1.5M-2.1M)', '标清质量(AVG：2M-5.1M)', '高清质量(AVG：5M-10.1M)',
                       '超高质量(AVG：10M-15.1M)', '顶级质量(AVG：15M-20.1M)', '巨顶质量(AVG：大于20M)',
                       '含有动图GIF']
    for root, dirs, files in os.walk(rootDir):
        dir_list = dirs
        break
    for i in dir_list:
        if i in DocumentQuality:
            resultList.append(rootDir + '\\' + i)
    return resultList


def PC_Moble(targetDir):
    fileList = []
    for root, dirs, files in os.walk(targetDir):
        fileList = files
        break
    if not os.path.exists(targetDir + '\\手机壁纸'):
        os.mkdir(targetDir + '\\手机壁纸')
    if not os.path.exists(targetDir + '\\电脑壁纸'):
        os.mkdir(targetDir + '\\电脑壁纸')
    for file in fileList:
        if getImgResolution(targetDir + '\\' + file) < 0:
            os.remove(targetDir + '\\' + file)
        else:
            if getImgResolution(targetDir + '\\' + file):
                shutil.move(targetDir + '\\' + file, targetDir + '\\电脑壁纸')
            else:
                shutil.move(targetDir + '\\' + file, targetDir + '\\手机壁纸')
    print(f'{targetDir}移动完成')


if __name__ == '__main__':
    webStr = input('草料归档还是wall(0/1):')
    if webStr == '0':
        rootDir = 'E:\草料\其他\图集\无码写真'
        t66yStart(rootDir)
    elif webStr == '1':
        rootDir = 'E:\草料\其他\无敌最寂寞\GIF'
        # wallStart()
        PC_Moble(rootDir)
