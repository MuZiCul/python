import time

import DownLoadImg
import DB
import config
import utils

Fore = config.Fore


def start():
    print(Fore.GREEN + '开始运行：')
    URLlist = ['http://t66y.com/thread0806.php?fid=8&search=&page=1',
               'http://t66y.com/thread0806.php?fid=8&search=&page=2',
               'http://t66y.com/thread0806.php?fid=16&search=&page=1',
               'http://t66y.com/thread0806.php?fid=16&search=&page=2']
    successCount = 0
    failCount = 0
    UrlType = '洲]'
    UrlType1 = '真]'
    for URL in URLlist:
        response = utils.getResponse(URL, DownLoadImg.headers, 0)
        if not response:
            break
        # 正则匹配图片链接和主题名
        result = utils.getUrlList('<td class="tal" id=""> \r\n\r\n\t\r\n\r\n\t(.*?)\r\n\r\n\t<h3><a href="(.*?)" target="_blank" id="">(.*?)</a></h3>', str(response[1]))
        for res in result:
            if 'fid=8' in URL:
                if UrlType in res[0] or UrlType1 in res[0]:
                    if not dataBase.selectSuccessPageUrl(res[1]):
                        url = res[1]
                        if not DownLoadImg.DownLoadStart(f'http://t66y.com/{url}', config.localePath):
                            dataBase.insertFailPageUrl(url)
                            failCount += 1
                        else:
                            successCount += 1
                            dataBase.insertSuccessPageUrl(url)
            elif 'fid=16' in URL:
                if not dataBase.selectSuccessPageUrl(res[1]):
                    url = res[1]
                    if not DownLoadImg.DownLoadStart(f'http://t66y.com/{url}', config.localePathSelf):
                        dataBase.insertFailPageUrl(url)
                        failCount += 1
                    else:
                        successCount += 1
                        dataBase.insertSuccessPageUrl(url)

    print(Fore.BLUE + f'\n本轮统计：\n成功{successCount}个，失败{failCount}个。')


if __name__ == '__main__':
    dataBase = DB.Operation_mysql(True)
    start()
    dataBase.close_conn()
    print(Fore.PURPLE + '第一次监听结束，下次将在整点开始！并每隔一小时抓取一次网站。')
    while True:
        c_time = time.strftime("%H:%M:%S", time.localtime())  # 将本地时间转换为字符串，并格式化为 时：分：秒
        if c_time[3:5] == '00':  # 判断截取分钟是否为0
            if c_time[6:8] == '00':  # 判断截取秒是否为0
                print(Fore.YELLOW + '监听开始：')
                a = time.time()
                print(Fore.RED + f'现在时间：{c_time}')
                dataBase = DB.Operation_mysql(True)
                start()
                dataBase.close_conn()
                b = time.time()
                print(Fore.YELLOW + '监听结束，开始休眠！')
                time.sleep(3540-int(b-a))
                print(Fore.PURPLE + '休眠结束，程序已激活，程序将在下一个整点运行。')

