import copy
import random
import shutil
import requests
import re
import os
import time
import urllib3
import DownLoadImg


headers = {
    'authority': 'cl.hexie.xyz',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'accept-language': 'zh-CN,zh;q=0.9',
    'cookie': '227c9_lastvisit=0%091650830334%09%2Fspace.php%3Fuser%3D%E6%9D%8E%E5%A9%B7',
    "host": "t66y.com",
    'Connection':'keep-alive',
    "Content-Type": "application/json;charset=utf-8",
    'Accept-Encoding': 'gzip, deflate',
    'Upgrade-Insecure-Requests': '1'
}

img_header = {
    'Host': 't1.ledchuzu.com',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'If-None-Match': "626653a5-4c70f"
}

headers1 = {
    'Host': 'xn--0trs0db7pba982x1yd.zhaofeiyan.cf',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1'
}

headers2 = {
    'Host': 'xn--0trs0db7pba982x1yd.zhaofeiyan.cf',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1'
}


User_Agents = [
    "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/537.75.14",
    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)",
    'Mozilla/5.0 (Windows; U; Windows NT 5.1; it; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11',
    'Opera/9.25 (Windows NT 5.1; U; en)',
    'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
    'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Kubuntu)',
    'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
    'Lynx/2.8.5rel.1 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/1.2.9',
    "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Ubuntu/11.04 Chromium/16.0.912.77 Chrome/16.0.912.77 Safari/535.7",
    "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:10.0) Gecko/20100101 Firefox/10.0 "
]

FailUrlList = []


def DownLoadImg(url, path, SN, No):
    # 修改请求头，防止被服务器屏蔽
    ChangeHeaderImg()
    global FailUrlList
    try:
        # 获取图片后缀
        others = url.rsplit('.', 1)
        img_suffix = others[1]
        # 拼接图片真实存放地址
        if No == 1:
            rulePath = path + '\\' + str(others[0].rsplit('/', 1)[1]) + '.' + img_suffix
        else:
            rulePath = path + '\\' + SN + '.' + img_suffix
        urllib3.disable_warnings()
        r = requests.get(url, headers=img_header, verify=False, timeout=10)
        if r.status_code == 200:
            with open(rulePath, 'wb') as img_file:
                img_file.write(r.content)  # 写入二进制内容
                print(f"No.{str(SN).zfill(3)}:{url} ,Download Successful,Time Cost:{round(r.elapsed.total_seconds(), 5)}s,Status Code:{r.status_code}")
                r.close()
                time.sleep(1)
        else:
            print(f'\n下载第{SN}个：{url}时出错。将在1秒后开始下一个。拒绝访问\n')
            time.sleep(1)
            r.close()
            FailUrlList.append(url)
    except Exception as e:
        print(f'\n下载第{SN}个：{url}时出错。将在1秒后开始下一个。\n故障详情：{e}\n')
        time.sleep(1)
        FailUrlList.append(url)


def SetImgSavePath(name):
    # 创建目录文件夹
    localePath = 'D:\\草料\\轻质量'
    paths = localePath + '\\[' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + ']' + name
    # 获取目录下文件夹
    dir_list = []
    for root, dirs, files in os.walk(localePath):
        dir_list = dirs
        break
    dir_str = ''
    for i in dir_list:
        dir_str += i
    if name in dir_str:
        nama_index = dir_str.index(name)
        name_len = len(name)
        file_name = ''
        for i in range(nama_index - 30, nama_index + name_len):
            file_name += dir_str[i]
        format_path = localePath + '\\' + file_name
        if os.path.exists(format_path):
            print(f'文件夹{format_path}已存在，图片将存到本目录下！')
        else:
            os.mkdir(format_path)
            print(f'创建文件夹-->{format_path}<--成功！')
        return format_path
    else:
        os.mkdir(paths)
        print(f'创建文件夹-->{paths}<--成功！')
    return paths


def checkNet(urls):
    print('开始检查网络连通性：')
    rs = ''
    try:
        urllib3.disable_warnings()
        rs = requests.get(urls, headers=headers1, stream=True, verify=False, timeout=10)
    except Exception as es:
        print(f'网络异常，请检查！\n故障详情{es}')
        rs.close()
    if rs.reason == 'OK':
        print(f'网络正常，可以进行正常下载！\n状态码：{rs.status_code}')
        rs.close()
        return 1
    else:
        print(f'网络异常，请检查！\n故障详情{rs}')
        rs.close()


def DownLoadImgList(urlList, path):
    print(f'开始下载{len(urlList)}个文件：')
    No = len(urlList)
    for i in urlList:
        DownLoadImg(i, path, str(len(urlList) - No + 1), 0)
        No -= 1
    print(f'\n下载完成！'
          f'\n下载{len(urlList)}个图片，'
          f'\n下载成功：{len(urlList) - len(FailUrlList)}个，'
          f'\n下载失败：{len(FailUrlList)}个，'
          f'\n存放位置：{path}')


def GetImgList(pageUrl):
    # 修改请求头，防止被服务器屏蔽
    ChangeHeader2()
    # 爬取网页内容

    urllib3.disable_warnings()
    res = ''
    try:
        res = requests.get(pageUrl, headers=headers2, stream=True, verify=False,  timeout=10)
    except Exception as e:
        print(f'出现故障：\n{e}')
        print('将在3秒后重连！')
        time.sleep(3)
        GetImgList(pageUrl)
        return
    # 正则匹配图片链接和主题名
    urlLists = re.findall('<img src="(.*?)" alt=', res.text, re.S)
    imgTitle = re.findall('<h1 class="entry-title">(.*?)</h1>	</header>', res.text, re.S)
    res.close()
    return urlLists, imgTitle


def DownLoadStart(URL):
    global FailUrlList
    # 修改请求头，防止被服务器屏蔽
    ChangeHeader2()
    # 爬取网页内容
    urllib3.disable_warnings()
    res = ''
    try:
        res = requests.get(URL, headers=headers2, stream=True, verify=False, timeout=10)
    except Exception as e:
        print(f'出现故障：\n{e}')
        print('将在3秒后重连！')
        time.sleep(3)
        DownLoadStart(URL)
        return
    urlList = re.findall('<br />\n <a href="(.*?)" class="more-link">', res.text, re.S)
    urlList1 = re.findall(' /> <a href="(.*?)" class="more-link">', res.text, re.S)
    res.close()
    urlList.extend(urlList1)
    # 检查网络连通性
    if checkNet(urlList[0]):
        print(f'网络连通性检查完成！\n链接列表获取完成，共{len(urlList)}个链接！\n')
        count = 0
        for url in urlList:
            count += 1
            ImgUrlList, ImgTitle = GetImgList(url)
            ImgPath = SetImgSavePath(ImgTitle[0])
            DownLoadImgList(ImgUrlList, ImgPath)
            if len(FailUrlList) > 0:
                print(f'\n下载失败列表：')
                for i in FailUrlList:
                    print(i)
                faliCount = 0
                print('将在2秒后开始下载失败列表。')
                time.sleep(2)
                while len(FailUrlList) > 0:
                    if faliCount > 3:
                        FailUrlList.clear()
                        break
                    if not checkNet(FailUrlList[0]):
                        print('网络故障，将在2秒后重新开始下载失败列表。')
                        time.sleep(2)
                        continue
                    FailList = copy.deepcopy(FailUrlList)
                    FailUrlList.clear()
                    print(f'\n开始重新下载失败列表{len(FailList)}个文件：')
                    No = len(FailList)
                    for i in FailList:
                        DownLoadImg(i, ImgPath, str(len(FailList) - No + 1), 1)
                        No -= 1
                    faliCount += 1
            DownLoadImg.Judge_picture_quality(ImgPath)
            print(f'第{count}组下载完成，第{count+1}组将在2秒后开始下载。')
            time.sleep(2)
    else:
        print('\n网络连通性异常，请检查！')


def ChangeHeader():
    global headers
    headers["User-Agent"] = random.choice(User_Agents)


def ChangeHeader2():
    global headers2
    headers2["User-Agent"] = random.choice(User_Agents)


def ChangeHeaderImg():
    global img_header
    img_header["User-Agent"] = random.choice(User_Agents)


if __name__ == '__main__':
    tage = 'https://xn--0trs0db7pba982x1yd.zhaofeiyan.cf/'
    tic = time.time()
    DownLoadStart(tage)
    toc = time.time()
    shijian = toc - tic
    print(f'\n本次用时：{shijian}秒')
    # res = requests.get(tage, headers=headers2)
    # print(res.text)
