import copy
import requests
import re
import time
import concurrent.futures
import parsel
import os


headers1 = {
    'Host': 'm1.446677.xyz',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Connection': 'keep-alive',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'If-None-Match': "6262cf9d-607fc"
}
headers = {
    'Host': 'www.kanxiaojiejie.com',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,en-US;q=0.7,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate, br',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Connection': 'keep-alive',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
}


def date_fromat(date):
    if date == '0':
        return 0
    date = date.replace(" ", "")
    date = date[0:8]
    return date


def GetFile(paths):
    for files in os.walk(paths):
        return files[2]


def DownloadImg(img_url, date, paths):
    img_name = f'[{date}]' + img_url.split('/')[-1]
    if img_name not in GetFile(paths):
        rulePath = paths + '\\' + img_name
        try:
            r = requests.get(img_url, headers=headers1, timeout=(1, 15), )
            with open(rulePath, 'wb') as img_file:
                img_file.write(r.content)  # 写入二进制内容
                print(
                    f"{img_url} ,Download Successful,Time Cost:{round(r.elapsed.total_seconds(), 5)}s,Status Code:{r.status_code}")
                return 0
        except Exception as e:
            print(f'出现错误：{e}')
            return img_url
    else:
        print("文件已存在，跳过下载")


def main(html_url, date):
    FailHtmlUrlList = []
    paths = 'D:\\草料\\小姐姐'
    FailUrlList = []
    requests.DEFAULT_RETRIES = 10
    try:
        html_data = requests.get(html_url, headers=headers, timeout=(1, 20))
    except Exception as e:
        print(e)
    else:
        if html_data.status_code == 200:
            zip_data = re.findall('<a href="(.*?)" target="_blank"rel="bookmark">(.*?)</a>', html_data.text)
            for zip_url, title in zip_data:
                if int(date_fromat(title)) > int(date_fromat(date)):
                    html_data_2 = requests.get(url=zip_url, headers=headers, timeout=(1, 20))
                    selector = parsel.Selector(html_data_2.text)
                    img_list = selector.css('p>img::attr(src)').getall()
                    for img_url in img_list:
                        fail = DownloadImg(img_url, date_fromat(title), paths)
                        if fail != 0 and fail is not None:
                            FailUrlList.append(fail)
                if len(FailUrlList) > 0:
                    print("失败列表：")
                    for i in FailUrlList:
                        print(i)
                    while len(FailUrlList) > 0:
                        FailList = copy.deepcopy(FailUrlList)
                        FailUrlList.clear()
                        print(f'开始重新下载失败列表{len(FailList)}个文件：')
                        for img_url in FailList:
                            fail = DownloadImg(img_url, date_fromat(title), paths)
                            if fail != 0 and fail is not None:
                                FailUrlList.append(fail)
        else:
            FailHtmlUrlList.append(html_url)
            print("帖子访问出错")
        if len(FailUrlList) > 0 :
            print("失败页面：")
            for i in FailHtmlUrlList:
                print(i)


if __name__ == '__main__':
    time_1 = time.time()
    exe = concurrent.futures.ThreadPoolExecutor(max_workers=20)
    date = input('请指定日期节点，只检索指定日期后的数据，格式为yyyymmdd，输入0跳过：')
    for page in range(1, 200):
        url = f'https://www.kanxiaojiejie.com/page/{page}'
        exe.submit(main, url, date)
    exe.shutdown()
    main('https://www.kanxiaojiejie.com', '0')
    time_2 = time.time()
    use_time = int(time_2) - int(time_1)
    print(f'总计耗时:{use_time}秒')
