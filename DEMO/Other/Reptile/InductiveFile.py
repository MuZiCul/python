import os
import shutil
import time
import ImgResolution


def Judge_picture_quality(start_path):
    total_size = 0
    total = 0
    quality = ''
    gif = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            if '.gif' in f:
                gif = 1
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total += 1
                total_size += os.path.getsize(fp)
    totalMb = round(total_size/1048576, 2)
    filesize = round(totalMb/total, 2)

    if filesize < 0.11:
        quality = '垃圾质量'
    elif 0.1 < filesize < 0.6:
        quality = '劣质质量'
    elif 0.5 < filesize < 1.6:
        quality = '一般质量'
    elif 1.5 < filesize < 2.1:
        quality = '清晰质量'
    elif 2 < filesize < 5.1:
        quality = '高清质量'
    elif 5 < filesize < 10.1:
        quality = '稍高质量'
    elif 10 < filesize < 15.1:
        quality = '超高质量'
    elif 15 < filesize < 20.1:
        quality = '顶级质量'
    elif 20 < filesize:
        quality = '巨顶质量'
    if gif == 1:
        quality = '含有动图'
    start_path_rsplit = start_path.rsplit('\\', 1)
    dir_path = start_path_rsplit[1]
    new_path = start_path_rsplit[0] + f'\\[{quality}][AVG：{filesize}MB]' + '[' + time.strftime('%Y-%m-%d', time.localtime(time.time())) + ']'
    if '质量' in start_path and 'AVG' in start_path:
        file_name = ''
        for i in range(30, len(dir_path)):
            file_name += dir_path[i]
        new_path += file_name
    else:
        new_path += dir_path
    os.rename(start_path, new_path)
    Inductive(root_directory, new_path, filesize, gif)
    print(f'\n图片综合质量评价：\n 图片总数：{total}个\n 整体质量：{totalMb}MB\n 平均质量：{filesize}MB\n 综合评价：{quality}')


# 按图片质量归类
def Inductive(root_directory, path, filesize, gif):
    Inductive_Catalog = root_directory
    if filesize < 0.11:
        Inductive_Catalog = '垃圾质量(AVG：小于0.1M)'
    elif 0.1 < filesize < 0.6:
        Inductive_Catalog = '劣质质量(AVG：0.1M-0.6M)'
    elif 0.5 < filesize < 1.6:
        Inductive_Catalog = '一般质量(AVG：0.5M-1.6M)'
    elif 1.5 < filesize < 2.1:
        Inductive_Catalog = '清晰质量(AVG：1.5M-2.1M)'
    elif 2 < filesize < 5.1:
        Inductive_Catalog = '高清质量(AVG：2M-5.1M)'
    elif 5 < filesize < 10.1:
        Inductive_Catalog = '稍高质量(AVG：5M-10.1M)'
    elif 10 < filesize < 15.1:
        Inductive_Catalog = '超高质量(AVG：10M-15.1M)'
    elif 15 < filesize < 20.1:
        Inductive_Catalog = '顶级质量(AVG：15M-20.1M)'
    elif 20 < filesize:
        Inductive_Catalog = '巨顶质量(AVG：大于20M)'
    if gif == 1:
        Inductive_Catalog = '含有动图GIF'
    realPath = root_directory + '\\' + Inductive_Catalog
    if os.path.exists(realPath):
        shutil.move(path, realPath)
    else:
        os.mkdir(realPath)
        shutil.move(path, realPath)
    print(f'已归入文件夹：{realPath}')


def Inductivess(root_directory, filename):
    if '垃圾质量' in filename and '[2022' in filename:
        Inductive_Catalog = '垃圾质量(AVG：小于0.1M)'
    elif '劣质质量' in filename and '[2022' in filename:
        Inductive_Catalog = '劣质质量(AVG：0.1M-0.6M)'
    elif '一般质量' in filename and '[2022' in filename:
        Inductive_Catalog = '一般质量(AVG：0.5M-1.6M)'
    elif '清晰质量' in filename and '[2022' in filename:
        Inductive_Catalog = '清晰质量(AVG：1.5M-2.1M)'
    elif '标清质量' in filename and '[2022' in filename:
        Inductive_Catalog = '高清质量(AVG：2M-5.1M)'
    elif '高清质量' in filename and '[2022' in filename:
        Inductive_Catalog = '稍高质量(AVG：5M-10.1M)'
    elif '超高质量' in filename and '[2022' in filename:
        Inductive_Catalog = '超高质量(AVG：10M-15.1M)'
    elif '顶级质量' in filename and '[2022' in filename:
        Inductive_Catalog = '顶级质量(AVG：15M-20.1M)'
    elif '巨顶质量' in filename and '[2022' in filename:
        Inductive_Catalog = '巨顶质量(AVG：大于20M)'
    elif '含有动图' in filename and '[2022' in filename:
        Inductive_Catalog = '含有动图GIF'
    elif '质量' not in filename and '[2022' in filename:
        Judge_picture_quality(root_directory + '\\' + filename)
        return
    else:
        return
    realPath = root_directory + '\\' + Inductive_Catalog
    path = root_directory + '\\' + filename
    if os.path.exists(realPath):
        shutil.move(path, realPath)
    else:
        os.mkdir(realPath)
        shutil.move(path, realPath)
    print(f'已归入文件夹：{realPath}')


if __name__ == '__main__':
    # Start()
    dir_list = []
    root_directory = 'E:\草料\其他\反差\AAAA'

    for root, dirs, files in os.walk(root_directory):
        dir_list = dirs
        break
    for i in dir_list:
        Judge_picture_quality(root_directory + '\\' + i)