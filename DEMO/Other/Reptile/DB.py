import time

import pymysql
from dbutils.pooled_db import PooledDB
from dbutils.persistent_db import PersistentDB

import config
import utils


class Operation_mysql:

    def __init__(self, is_mult_thread=True):
        self.result = ''
        self.Fore = config.Fore
        self.is_mult_thread = is_mult_thread
        self.conn = self.get_db_pool().connection()
        self.cursor = self.conn.cursor()

    def get_db_pool(self):
        while True:
            try:
                if self.is_mult_thread:
                    poolDB = PooledDB(
                        # 指定数据库连接驱动
                        creator=pymysql,
                        # 连接池允许的最大连接数,0和None表示没有限制
                        maxconnections=3,
                        # 初始化时,连接池至少创建的空闲连接,0表示不创建
                        mincached=2,
                        # 连接池中空闲的最多连接数,0和None表示没有限制
                        maxcached=5,
                        # 连接池中最多共享的连接数量,0和None表示全部共享(其实没什么卵用)
                        maxshared=3,
                        # 连接池中如果没有可用共享连接后,是否阻塞等待,True表示等等,
                        # False表示不等待然后报错
                        blocking=True,
                        # 开始会话前执行的命令列表
                        setsession=[],
                        # ping Mysql服务器检查服务是否可用
                        ping=0,
                        **config.config
                    )
                else:
                    poolDB = PersistentDB(
                        # 指定数据库连接驱动
                        creator=pymysql,
                        # 一个连接最大复用次数,0或者None表示没有限制,默认为0
                        maxusage=1000,
                        **config.config
                    )
                print(self.Fore.GREEN + utils.getLocalTime() + 'DataBase：数据库连接成功，数据库连接池创建完成！\n')
                return poolDB
            except Exception as e:
                print(self.Fore.RED + utils.getLocalTime() + 'DataBase：连接数据库出现错误，将在五秒后重连，错误详情' + str(e))
                time.sleep(5)

    # 获取一条数据库链接
    def get_conn(self):
        conn = self.conn
        cur = self.cursor
        return conn, cur

    # 关闭数据库链接
    def close_conn(self):
        self.cursor.close()
        self.conn.close()
        print(self.Fore.YELLOW + utils.getLocalTime() + 'DataBase：数据库关闭成功，数据库连接池释放完成！\n')

    def insertSuccessPageUrl(self, value):
        conn, cursor = self.get_conn()
        sql = """insert into spu (url) values (%s)"""
        print(self.Fore.BLUE + f'DataBase：{sql % value}')
        try:
            self.result = cursor.execute(sql, value)
            conn.commit()
        except Exception as e:
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：执行SQL语句出现错误，详情' + str(e))
            conn.rollback()
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：已回滚')
        else:
            print(self.Fore.GREEN + utils.getLocalTime() + f'DataBase：SQL执行成功,RESULT--->{self.result}\n')
        return self.result

    def insertFailPageUrl(self, value):
        conn, cursor = self.get_conn()
        sql = """insert into fpu (url) values (%s)"""
        print(self.Fore.BLUE + utils.getLocalTime() + f'DataBase：{sql % value}')
        try:
            self.result = cursor.execute(sql, value)
        except Exception as e:
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：执行SQL语句出现错误，详情' + str(e))
            conn.rollback()
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：已回滚')
        else:
            print(self.Fore.GREEN + utils.getLocalTime() + f'DataBase：SQL执行成功,RESULT--->{self.result}\n')
        return self.result

    def insertFailImgUrl(self, value):
        conn, cursor = self.get_conn()
        sql = """insert into fiu (url) values (%s)"""
        print(self.Fore.BLUE + utils.getLocalTime() + f'DataBase：{sql % value}')
        try:
            self.result = cursor.execute(sql, value)
            conn.commit()
        except Exception as e:
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：执行SQL语句出现错误，详情' + str(e))
            conn.rollback()
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：已回滚')
        else:
            print(self.Fore.GREEN + utils.getLocalTime() + f'DataBase：SQL执行成功,RESULT--->{self.result}\n')
        return self.result

    def selectSuccessPageUrl(self, value):
        conn, cursor = self.get_conn()
        sql = f"""select * from spu where url like %s """
        value = '%' + value + '%'
        print(self.Fore.BLUE + utils.getLocalTime() + f'DataBase：{sql % value}')
        try:
            cursor.execute(sql, value)
            self.result = cursor.fetchall()
        except Exception as e:
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：执行SQL语句出现错误，详情' + str(e))
        else:
            print(self.Fore.GREEN + utils.getLocalTime() + f'DataBase：SQL执行成功,RESULT--->{self.result}\n')
        return self.result

    def selectSuccessPageUrlAll(self):
        conn, cursor = self.get_conn()
        sql = f"""select * from spu"""
        print(self.Fore.BLUE + utils.getLocalTime() + f'DataBase：{sql}')
        try:
            cursor.execute(sql)
            self.result = cursor.fetchall()
        except Exception as e:
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：执行SQL语句出现错误，详情' + str(e))
        else:
            print(self.Fore.GREEN + utils.getLocalTime() + f'DataBase：SQL执行成功,RESULT--->{self.result}\n')
        return self.result

    def deleteFailPageUrl(self, value):
        conn, cursor = self.get_conn()
        sql = f"""delete from spu where url = %s"""
        print(self.Fore.BLUE + utils.getLocalTime() + f'DataBase：{sql % value}')
        try:
            self.result = cursor.execute(sql, value)
            conn.commit()
        except Exception as e:
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：执行SQL语句出现错误，详情' + str(e))
            conn.rollback()
            print(self.Fore.RED + utils.getLocalTime() + 'DataBase：已回滚')
        else:
            print(self.Fore.GREEN + utils.getLocalTime() + f'DataBase：SQL执行成功,RESULT--->{self.result}\n')
        return self.result



