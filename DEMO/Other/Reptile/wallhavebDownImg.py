import os
import re
import shutil
import time


import config
import DB
import utils


Fore = config.Fore

cookie = '''cf_clearance=TuTnIqnIXgNDDwReAMyAatXaEVtGu7lf.keW4RyKU7c-1651409979-0-250; remember_web_59ba36addc2b2f9401580f014c7f58ea4e30989d=eyJpdiI6IlNDOEV3Q3J1d2ZCRk0zQ2hzczIyZFE9PSIsInZhbHVlIjoiN3Q3QlZYNWpXNHd0ajlUbmFUbkhyRVQxcFlXUXZDT0pzb2pCWUlsaVFoQ1k4VmpSNzRrM0M0ZmlSQVVjcHZ6V0k4NlwveVlBNXl1aVp3M0I3S09QR1wvS0QzaXFiczlrUVFIMXE0ZGlBaUJtcjZnUkpSZGNaOHM2cStEV2tWaEMyQmR0SFFmN3dIbk9jdDhYQllPU0hsdDIwbXZnSGd6ODJKdUFSRnRUSnp3bkQ4ZXhraUVwbDdxZVpsa3lTTnNHNHoiLCJtYWMiOiJjZWI4NDg3NDFlMzQwMzBhOTg1MGNjMGZiOGY0OWM5NTUxNDc1MjE4NzkxZjAxY2I3MDY1YjFiOTBkOGU4YmFiIn0%3D; _pk_id.1.01b8=266d9814ed0d30c8.1651413523.; XSRF-TOKEN=eyJpdiI6IkhoWDBoZHF4UkxVWHVydGxGQ0UrZkE9PSIsInZhbHVlIjoiTEcxK3RMSE9yT1wvajhsTlhsM2xQbDNockJ6a0oxMFl1V3Z4aktoYVhXdUkrcTVGQ2YrNTZlZkwxM1FONmNHMisiLCJtYWMiOiI1ZTMwOTFlYTdmMGJhMGNmNTFjNjhiNDQxNjQ3ODgwOTc3Y2I5M2M4ZjA2MTVlODNiZGIwNTcwN2RlNGZmMjRiIn0%3D; wallhaven_session=eyJpdiI6IlljbUpXVUtsXC9JdWpjeERRYnJKTXRBPT0iLCJ2YWx1ZSI6IkRuYVE5dXRDWEZuMjA4aHptekt0dEFcL1hoZ3U4bU4ydWF4VGJsRGMrcjlxUktUTkhuMGUzbkY0NmJ4eWx1SHhBIiwibWFjIjoiMzE1Yzc0MDRhMDgzMmEwZDZlMmY4YzIxYWNmYjg0ZjcwM2E1MDBiYThkODY0M2ZiMzM2NGI3MGY5ZWMzMTQ2MSJ9'''

headers = {
    'cookie': cookie,
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0',
}


def getImgUrlList(PrUrl, urlCount):
    if urlCount != 0:
        URL = PrUrl + str(urlCount)
    else:
        URL = PrUrl
    response = utils.getResponse(URL, headers, 0)
    if response:
        urls = utils.getUrlList('class="preview" href="(.*?)"', str(response[1]))
        print(Fore.YELLOW + f'第{urlCount}页共获取到{len(urls)}个url')
        count = 1
        for imgUrl in urls:
            if utils.judgeUrlDownloadSuccess(dataBase, imgUrl):
                continue
            else:
                print(Fore.BLUE + f'第{urlCount}页, Download No.{count}---->{imgUrl}\n')
                if getImgUrl(imgUrl):
                    print(Fore.BLUE + f'SUCCESS No.{count}----->{imgUrl}\n')
                else:
                    dataBase.insertFailImgUrl(imgUrl)
                    return False
            count += 1
        return True
    else:
        return False


def getImgUrl(imgUrl):
    if utils.judgeUrlDownloadSuccess(dataBase, imgUrl):
        return False
    response = utils.getResponse(imgUrl, headers, 0)
    if response:
        img_url = utils.getUrlList('<img id="wallpaper" src="(.*?)"', str(response[1]))[0]
        title = img_url.split('-')[-1]
        if download(title, img_url):
            return False
        dataBase.insertSuccessPageUrl(imgUrl)
        return True
    else:
        return False


def download(title, ImgUrl):
    path = config.wallImgPath + '\\' + title
    if not os.path.exists(path):
        response = utils.getResponse(ImgUrl, headers, 1)
        if response:
            with open(path, mode='wb') as f:
                f.write(response[2])
            Inductive(path)
        else:
            return False
    else:
        print(Fore.RED + f'文件{path}已存在，操作已回退！')
        return False


# 按图片质量归类
def Inductive(PrFilePath):
    fSize = os.path.getsize(PrFilePath)  # 获取文件大小：以字节为单位
    # 可换算成MB等单位
    filesize = round(fSize / float(1024 * 1024), 2)
    print(Fore.YELLOW + f'\n文件大小{filesize}MB')

    oldPathRsplit = PrFilePath.rsplit('\\', 1)
    Inductive_Catalog = utils.inductive(filesize, oldPathRsplit, 0)

    targetPath = oldPathRsplit[0] + '\\' + Inductive_Catalog
    targetDir = targetPath + '\\' + oldPathRsplit[1]
    if os.path.exists(targetDir):
        print(Fore.RED + f'归档文件已存在，已停止归档')
        return
    else:
        if os.path.exists(targetPath):
            shutil.move(PrFilePath, targetPath)
        else:
            os.mkdir(targetPath)
            shutil.move(PrFilePath, targetPath)

    if not os.path.exists(targetPath + '\\手机壁纸'):
        os.mkdir(targetPath + '\\手机壁纸')
    if not os.path.exists(targetPath + '\\电脑壁纸'):
        os.mkdir(targetPath + '\\电脑壁纸')
    if os.path.exists(targetPath + '\\电脑壁纸' + oldPathRsplit[1]):
        print(Fore.RED + f'归档文件已存在，已停止归档')
        return
    if utils.getImgResolution(targetDir):
        shutil.move(targetDir, targetPath + '\\电脑壁纸')
    else:
        shutil.move(targetDir, targetPath + '\\手机壁纸')
    print(Fore.yellow + f'已归入文件夹：{Inductive_Catalog}\n')


def start():
    action = input(Fore.GREEN + '单点or全站(0/1)：')
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    if action == '1':
        while True:
            pageStart = input(Fore.YELLOW + '起始页码：')
            pageEnd = input(Fore.YELLOW + '结束页码：')
            url = input(Fore.YELLOW + '不带页码的URL：')
            if pageStart.isdigit() and pageEnd.isdigit() and re.match(regex, url) is not None:
                break
            else:
                print(Fore.RED + '输入有误，请检查后重新输入！')
                start()
        for i in range(int(pageStart), int(pageEnd) + 1):
            print(Fore.GREEN + f'开始下载第{i}页\n')
            if getImgUrlList(url, i):
                print(Fore.GREEN + f'第{i}页下载完成，休眠五秒后开始{i+1}页\n')
                time.sleep(5)
            else:
                dataBase.insertFailPageUrl(url + str(i))
        start()
    else:
        while True:
            URL = input(Fore.GREEN + '请输入要下载的URL(按0退出)：')
            if URL == '0':
                start()
            if re.match(regex, URL) is not None:
                if not getImgUrl(URL):
                    dataBase.insertFailImgUrl(URL)
            else:
                print(Fore.RED + '输入有误，请检查后重新输入！')
                continue


if __name__ == '__main__':
    dataBase = DB.Operation_mysql(True)
    start()
    dataBase.close_conn()



