import os

if __name__ == '__main__':

    tage = input('要批量修改文件还是文件名，0为文件，1为文件名（默认为都修改），请输入：')
    while tage != '1' and tage != '0':
        tage = input('要批量修改文件还是文件名，0为文件，1为文件名（默认为都修改），请输入：')

    path = input('请输入文件路径(结尾加上/)：')
    # 获取该目录下所有文件，存入列表中
    T = True
    fileList = []
    while T:
        try:
            fileList = os.listdir(path)
            T = False
        except Exception as e:
            print(e)
            path = input('请输入文件路径(结尾加上/)：')
            T = True

    filelist = []
    for file_name in fileList:
        if tage == "0":
            if not os.path.isdir(path + os.sep + file_name):
                filelist.append(file_name)
        elif tage == "1":
            if os.path.isdir(path + os.sep + file_name):
                filelist.append(file_name)

    print("\33[1;35m获取到的文件列表：\33[0m")
    for file_name in filelist:
        print(file_name)

    oname = input('请输入要替换的文字：')
    nname = input('请输入替换后的文字：')
    if nname is None:
        nname = ""
    if oname is None:
        oname = ""

    print("\33[1;35m修改文件信息列表：\33[0m")
    for file in filelist:
        # 设置旧文件名（就是路径+文件名）
        oldname = path + os.sep + file  # os.sep添加系统分隔符

        # 设置新文件名
        newname = path + os.sep + file.replace(oname, nname)

        os.rename(oldname, newname)  # 用os模块中的rename方法对文件改名
        print(file, '\33[1;35m======>\33[0m', file.replace(oname, nname))
